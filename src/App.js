import React, { Component } from 'react';
import TokenBalance from './components/TokenBalance';
import logo from './logo.svg';
import './App.css';

/**
 * Golem token contract
 * @type {string}
 * @see https://etherscan.io/token/Golem
 */
const TOKEN_ADDRESS = '0xa74476443119A942dE498590Fe1f2454d7D4aC0d';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>React + Ethereum token contract</h2>
        </div>
        <p className="App-intro">
          Example of the component usage: <code>{`<TokenBalance tokenAddress={TOKEN_ADDRESS}/>`}</code>.
        </p>
        <TokenBalance tokenAddress={TOKEN_ADDRESS}/>
      </div>
    );
  }
}

export default App;
