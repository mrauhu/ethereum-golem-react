import React, {Component} from 'react';
import Eth, { HttpProvider } from 'ethjs';
import HumanStandardTokenABI from 'human-standard-token-abi';
import './TokenBalance.css';

/**
 * Ethereum node HTTPS endpoint
 * @type {{PROD: string, DEV: string}}
 * @see https://infura.io/docs/
 */
const ENDPOINT = {
  PROD: 'https://mainnet.infura.io/',
  DEV: 'https://ropsten.infura.io/',
};

const FRACTIONAL_DIGITS = 3;

/**
 * Form for getting balance of the token contract address
 * based on EIP20 Token standard
 * @see https://github.com/ethereum/eips/issues/20
 */
export default class TokenBalance extends Component {
  /**
   * TokenBalance constructor
   * @param {Object} props options
   * @param {string} props.tokenAddress Token address
   * @param {string} [props.endpoint='PROD'] Ethereum node HTTPS endpoint: PROD or DEV
   */
  constructor(props) {
    super(props);

    this.state = {
      info: {
        name: '',
        symbol: '',
        totalSupply: '',
      },
      address: '',
      balance: '',
    };

    this.handleSubmit = this.handleSubmit.bind(this);

    let {tokenAddress, endpoint} = props;
    endpoint = typeof endpoint !== 'undefined'
      ? endpoint
      : ENDPOINT.PROD;

    const eth = new Eth(new HttpProvider(endpoint));
    this.token = eth.contract(HumanStandardTokenABI).at(tokenAddress);
  }

  /**
   * Handle submit
   * @param event
   * @return {Promise.<void>}
   */
  async handleSubmit(event) {
    event.preventDefault();
    //console.log('handleSubmit event', event);
    this.setState({
      balance: await this.balanceOf( event.target.address.value )
    });
  }

  /**
   * Get token info
   * @return {Promise.<{name: *, symbol: *, totalSupply: (string|*)}>}
   */
  async getTokenInfo() {
    let token = this.token;
    //console.log('token', token);
    let name = await token.name();
    let symbol = await token.symbol();
    let totalSupply = await token.totalSupply();

    return {
      name: name[0],
      symbol: symbol[0],
      totalSupply: totalSupply[0].toString(),
    };
  }

  /**
   * Balance of address
   * @param address
   * @return {Promise.<string>}
   */
  async balanceOf(address) {
    //console.log('balanceOf()', typeof address, address);
    try {
      let balance_obj = await this.token.balanceOf(address);
      //console.log('balanceOf() - balance', balance_obj );
      let balance = balance_obj[0].toString();
      let percent = Number((balance / this.state.info.totalSupply) * 100).toFixed(FRACTIONAL_DIGITS);

      return balance
        + ' ' + this.state.info.symbol
        + ' — ' + percent + '%'
        ;
    } catch (e) {
      console.error('balanceOf() - error', e);
      return e.message;
    }
  }

  /**
   *
   * @return {Promise.<void>}
   */
  async componentDidMount() {
    try {
      let info = await this.getTokenInfo();
      this.setState({
        info
      });
    } catch (e) {
      console.error('getTokenInfo() - error', e);
    }
  }

  render() {
    return (
      <section className="TokenBalance">
        <h1>{this.state.info.name}</h1>
        <h3>{this.state.info.totalSupply} {this.state.info.symbol}</h3>
        <form className="TokenBalance-form" onSubmit={this.handleSubmit}>
          <input type="text" placeholder="Enter address" name="address" />
          <input type="text" disabled={true} value={this.state.balance} />
          <input type="submit" value="Check" />
        </form>
      </section>
    );
  }

}
