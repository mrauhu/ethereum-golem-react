This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

# React + Ethereum token contract Golem

> Example of work with the Ethereum node from Javascript

## Prerequisites

* Latest version of Node.js
* Optionally you need `yarn` package manage

## Installation

```bash
  npm install
```

or

```bash
  yarn install
```

## Running

```bash
  npm run
```